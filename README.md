# Vertical Float Sections

**Author:** Hemant Arora
**Company:** TivLabs

A javascript code to vertically float a section and keep it fixed at a position
while the user scrolls and keep it constrained within a container.


## Include the Javascript files

Vertical Float Sections works on jQuery. Please inclide the latest version of
jQuery library.

After jQuery, include `vertical-float-sections.js` in the `<head>` of your HTML.

```
<script type="text/javascipt" href="/path/to/your/vertical-float-sections.js"></script>
```


## Use the following HTML structure:

Use the HTML tag attribute `data-vfscontain` to specify the parent container,
the floating element needs to stay within.

Next, use the class `vfs-float` to specify a floating element.

Specify the floating method by adding one of the following classes to the floating element:
* `vfs-float-via-margin` (Default)
* `vfs-float-via-position`

```
<div data-vfscontain="#my-floating-div1,#another-div2,...">
    ...
	<div id="#my-floating-div1" class="vfs-float vfs-float-via-margin"
	    data-vfsoffset="20">
	    ...
	</div>
	...
</div>
```

Any number of floating elements can be added inside the parent container element.

The HTML attribute `data-vfsoffset` is optional, and can be used to specify an
extra offset value.


## Configuration

Vertical float sections can be further customized by editing the following
variables in the javascript file:

* Use the script variable `vfs_min_device_width` to specify the minimum resolution required to enable the script, useful in disabling the floating in mobile/handheld devices.
* Use the script variable `vfs_floating_header_selector` to define a floating header (used for offset)
