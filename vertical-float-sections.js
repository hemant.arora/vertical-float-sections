var vfs_min_device_width = 768,
    vfs_floating_header_selector = '.site-header';

jQuery(window).load(function() {
  
  // Check device width and attach vertical scrolling to the window scroll event
  jQuery(window).scroll(function() {
    if(jQuery(window).width() >= vfs_min_device_width) {
      var scrollTop = jQuery(window).scrollTop(),
          window_height = jQuery(window).height(),
          header_height = (vfs_floating_header_selector && jQuery(vfs_floating_header_selector).length > 0) ? jQuery(vfs_floating_header_selector).height() : 0;
      jQuery('.vfs-float').each(function(i, float_item) {
        var $float_item = jQuery(float_item),
            $container = $float_item.closest('[data-vfscontain]');
        if($float_item.hasClass('vfs-float-via-position')) {
          if(!$float_item.hasClass('vfs-init')) {
            $container.css({ position: 'relative' });
            $float_item.data('vfsinitoffset', $float_item.offset());
            $float_item.css({
              position: 'fixed',
              top: $float_item.data('vfsinitoffset').top,
              bottom: 'auto',
              left: $float_item.data('vfsinitoffset').left,
            }).addClass('vfs-init');
          }
        }
      });
      if(typeof(window.vfs_init_callback) == 'function') window.vfs_init_callback();
      jQuery('.vfs-float').each(function(i, float_item) {
        var $float_item = jQuery(float_item),
            $container = $float_item.closest('[data-vfscontain]'),
            float_item_offset = (typeof($float_item.data('vfsoffset')) != 'undefined' ? parseInt($float_item.data('vfsoffset')) : ($container.offset().top - header_height)),
        	element_css = {};
        if((scrollTop + header_height) > $container.offset().top) {
          if($float_item.hasClass('vfs-float-via-position')) {
            if(!$float_item.hasClass('vfs-init')) {
              $container.css({ position: 'relative' });
              $float_item.data('vfsinitoffset', $float_item.offset()).css({ top: $float_item.offset().top, position: 'fixed' }).addClass('vfs-init');
            }
            element_css.position = 'fixed';
            element_css.top = header_height + float_item_offset;
            element_css.bottom = 'auto';
            element_css.left = $float_item.data('vfsinitoffset').left;
            if((scrollTop + window_height) > ($container.offset().top + $container.height())) {
              element_css.position = 'absolute';
              element_css.top = 'auto';
              element_css.bottom = 0;
			  element_css.left = $float_item.data('vfsinitoffset').left - $float_item.closest('[data-vfscontain]').offset().left;
            }
          } else {
            if($float_item.hasClass('vfs-float-via-margin') || 1) {
              element_css.marginTop = (scrollTop + header_height) - $container.offset().top + float_item_offset;
              if(element_css.marginTop > ($container.height() - $float_item.height()))
                element_css.marginTop = ($container.height() - $float_item.height());
            }
          }
        } else {
          if($float_item.hasClass('vfs-float-via-position')) {
            element_css.position = 'fixed';
            element_css.top = $float_item.data('vfsinitoffset').top;
            element_css.bottom = 'auto';
            element_css.left = $float_item.data('vfsinitoffset').left;
          } else {
            if($float_item.hasClass('vfs-float-via-margin') || 1) {
              element_css.marginTop = 0;
            }
          }
        }
        $float_item.css(element_css);
      });
    }
  });
  
  // Check device width on window resize, do the needful adjustments
  jQuery(window).resize(function() {
    if(jQuery(window).width() >= vfs_min_device_width) {
      jQuery('.vfs-float').each(function(i, float_item) {
        var $float_item = jQuery(float_item);
        $float_item
          .removeData('vfsinitoffset')
          .css({
            position: 'relative',
            top: 'auto',
            bottom: 'auto',
            left: 'auto',
          })
          .removeClass('vfs-init');
      });
      if(typeof(window.vfs_init_callback) == 'function') window.vfs_init_callback();
      jQuery(window).trigger('scroll');
    }
  });
  
});